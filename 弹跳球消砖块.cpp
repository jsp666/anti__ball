#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)
#include"math.h"
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<windows.h>
//全局变量
int high, width;//范围
int ball_x, ball_y;//小球位置
int ban_x, ban_y;//挡板中心
int vx, vy;//球速
int radius;//半径
int enemy_x1, enemy_y1;
int enemy_x2, enemy_y2;
int enemy_x3, enemy_y3;
int score;
int i1;
void HideCursor()//光标隐藏
{
	CONSOLE_CURSOR_INFO cursor_info = { 1, 0 };
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}
void gotoxy(int x, int y)//类似于清屏函数，光标移动到原点位置进行重画
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle, pos);
}
void startup() // 数据初始化
{
	high = 28;
	width = 30;
	ball_x = 1;
	ball_y = width/2;
	vx = 1;
	vy = 1;
	ban_x = high;
	ban_y = width / 2;
	radius = 5;
	enemy_x1 = 5;
	enemy_y1 = 10;
	enemy_x2 = 8;
	enemy_y2 = 12;
	enemy_x3 = 10;
	enemy_y3 = 18;		
	score = 0;
}
void show()  // 显示画面
{
	HideCursor();//光标隐藏
	gotoxy(0, 0);//类似于清屏函数，光标移动到原点位置进行重画
	int i, j;
	for (i = 1; i <= high; i++)
	{
		for (j = 1; j <= width; j++)
		{
			if (i == ball_x && j == ball_y)
				printf("o");
			else if (i == ban_x && j >= ban_y - radius && j < ban_y + radius)
				printf("*");
			else if (i == high)
				printf("_");
			else if (j == width)
				printf("|");
			else if (i == enemy_x1 && j == enemy_y1)
				printf("A");
			else if (i == enemy_x2 && j == enemy_y2)
				printf("B");
			else if (i == enemy_x3 && j == enemy_y3)
				printf("C");
			else printf(" ");
		}
		printf("\n");
	}
	printf("Score=%d\n", score);
}
void updateWithoutInput()  // 与用户输入无关的更新
{

	static int speed = 0;
	if (speed < 5)
		speed++;
	if (speed == 5)
	{
		ball_x += vx;
		ball_y += vy;
		if (ball_x == 0 || ball_x == high - 1 && ball_y >= ban_y - radius && ball_y <= ban_y + radius)
		{
			vx = -vx;
		}
		 if (ball_y<=0 || ball_y >= width)
		{
			vy = -vy;
		}
		else if (ball_x == enemy_x1 && ball_y == enemy_y1)
		{
			enemy_x1 = rand() % (high - 3);
			enemy_y1 = rand() % width;
			score+=3;
			printf("\a");
		}
		else if (ball_x == enemy_x2 && ball_y == enemy_y2)
		{
			enemy_x2 = rand() % (high - 3);
			enemy_y2 = rand() % width;
			score+=2;
			printf("\a");
		}
		else if (ball_x == enemy_x3 && ball_y == enemy_y3)
		{
			enemy_x3 = rand() % (high - 3);
			enemy_y3 = rand() % width;
			score++;
			printf("\a");
		}
		else if (ball_x == high && ball_y<ban_y - radius || ball_x == high && ball_y>ban_y + radius)
		{
			printf("Game Over!");
			exit(0);
		}

		speed = 0;
	}

}

void updateWithInput()  // 与用户输入有关的更新
{
	if (kbhit())
	{
		char input;
		input = getch();
		if (input == 'a')
		{
			ban_y--;
			if ( ball_x == high - 1 && ball_y >= ban_y - radius && ball_y <= ban_y + radius&&vy>=-3&&vy<=3)
			vy=vy - 1;
		}
		if (input == 'd')
		{
			ban_y++;
			if (ball_x == high - 1 && ball_y >= ban_y - radius && ball_y <= ban_y + radius && vy >= -3 && vy <= 3)
			vy = vy + 1;
		}
	}
}
void main()
{
	startup();  // 数据初始化	
	while (1) //  游戏循环执行
	{
		show();  // 显示画面
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();  // 与用户输入有关的更新
	}
}